﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aprengine_Resource_Packer
{
    class Program
    {
        static void Main(string[] args)
        {
            if (args[0] == "-h" || args[0] == "--help")
            {
                Console.WriteLine("Resource Packer for Aprengine - Packs all the resources for a game into archives that can be read from directly \n This tool is used to protect resources before release");
            }
        }
    }
}
