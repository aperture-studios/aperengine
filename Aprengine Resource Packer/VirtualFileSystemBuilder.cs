﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Security.Cryptography;

namespace Aprengine_Resource_Packer
{
    class VirtualFileSystemBuilder
    {
        public VirtualFileSystemBuilder(string outputFilename, string rootDir)
        {
            List<string> files = Directory.EnumerateFiles(rootDir, "*", SearchOption.AllDirectories).ToList();
            FileStream output = File.OpenWrite(outputFilename);
            FileStream header = File.OpenWrite("header.bin");
            FileStream body = File.OpenWrite("body.bin");
            Dictionary<string, long> offsets = new Dictionary<string, long>();
            Dictionary<string, byte[]> hashes = new Dictionary<string, byte[]>();

            long offset = 0;
            SHA1 sha = new SHA1CryptoServiceProvider();


            foreach (string file in files)
            {
                offsets.Add(file, offset);

                byte[] data = File.ReadAllBytes(file);
                hashes.Add(file, sha.ComputeHash(data));

                data = encryptData(data);
                body.Write(data, 0, data.Length);
                offset += data.Length;
            }

            foreach(string file in files)
            {
                header.Write(sha.ComputeHash(Encoding.UTF8.GetBytes(file)), 0, 20);
                header.Write(hashes[file], 0, hashes[file].Length);
                header.Write(BitConverter.GetBytes(offsets[file]), 0, 8);
            }

            header.Close();
            body.Close();

            output.Write(new byte[]{(byte)'L',(byte)'u',(byte)'l',(byte)'z'}, 0, 4);
            var tmp = File.ReadAllBytes("header.bin");
            output.Write(tmp, 0, tmp.Length);
            tmp = File.ReadAllBytes("body.bin");
            output.Write(tmp, 0, tmp.Length);
        }

        private static byte[] encryptData(byte[] input)
        {
            SHA1 sha = new SHA1CryptoServiceProvider();
            byte[] key = sha.ComputeHash(input);
            byte[] iv = sha.ComputeHash(key);
            byte[] encrypted;

            Aes aes = Aes.Create();
            aes.Key = key;
            aes.IV = iv;

            ICryptoTransform encryptor = aes.CreateEncryptor(aes.Key, aes.IV);

            // Create the streams used for encryption. 
            using (MemoryStream msEncrypt = new MemoryStream())
            {
                using (CryptoStream csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write))
                {
                    using (StreamWriter swEncrypt = new StreamWriter(csEncrypt))
                    {

                        //Write all data to the stream.
                        swEncrypt.Write(input);
                    }
                    encrypted = msEncrypt.ToArray();
                }
            }


            // Return the encrypted bytes from the memory stream. 
            return encrypted;
        }

    }
}
