﻿using OpenTK.Graphics.OpenGL4;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aperengine
{
    /// <summary>
    /// A texture that can be used with a FrameBuffer
    /// </summary>
    public class FrameBufferTexture : Texture
    {
        public FrameBufferTexture(int width, int height, PixelInternalFormat format = PixelInternalFormat.Rgba, OpenTK.Graphics.OpenGL4.PixelFormat pf = OpenTK.Graphics.OpenGL4.PixelFormat.Bgra, PixelType type = PixelType.UnsignedByte)
            : base(width, height, format, pf, type) { }

        public void BindToFrameBuffer(int texUnit)
        {
            FramebufferAttachment attach = (FramebufferAttachment)texUnit;
            GL.FramebufferTexture(FramebufferTarget.Framebuffer, attach, id, 0);
        }

        public static void UnBindFromFrameBuffer(int texUnit)
        {
            FramebufferAttachment attach = (FramebufferAttachment)texUnit;
            GL.FramebufferTexture(FramebufferTarget.Framebuffer, attach, 0, 0);
        }

    }
}
