﻿using Aperengine.Math;
using Aperengine.Prefabs;
using Aperengine.Shading;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aperengine
{
    public class Sky
    {
        Vector3 min, max, cur;
        float interval;
        private static Sphere sphere = new Sphere(1);
        private static ShaderProgram skyShader = new ShaderProgram(new VertexShader("Shaders/GBuffer"), new FragmentShader("Shaders/SkyDome"));

        public Vector2 Time;
        public Sky(Vector3 min, Vector3 max, float interval)
        {
            this.min = min;
            this.max = max;
            this.interval = interval;
            Time = new Vector2(12, 00);
        }

        public void Update(GraphicsContext context)
        {
            Time.Y++;
            if (Time.Y == 61)
            {
                Time.Y = 1;
                Time.X++;
            }

            if (Time.X == 24)
            {
                Time.X = 0;
            }

            if (Time.X < 12)
            {
                cur.X = min.X + ((Time.X * 60) + Time.Y) / (interval * 60);
                cur.Y = min.Y + ((Time.X * 60) + Time.Y) / (interval * 60);
                cur.Z = min.Z + ((Time.X * 60) + Time.Y) / (interval * 60);
            }
            else
            {
                cur.X = max.X - ((Time.X * 60) + Time.Y) / (interval * 60);
                cur.Y = max.Y - ((Time.X * 60) + Time.Y) / (interval * 60);
                cur.Z = max.Z - ((Time.X * 60) + Time.Y) / (interval * 60);
            }
        }

        public void Draw(GraphicsContext Context)
        {
            skyShader.SetShaderVector("col", cur/255f);
            sphere.Materials[0].Shader = skyShader;
            sphere.World = Matrix4.Scale(Context.ZFar) * Matrix4.CreateTranslation(Context.Camera.Position);
            Context.DepthWrite(false);
            sphere.Draw(Context);
            Context.DepthWrite(true);
        }
    }
}
