﻿using Aperengine.Math;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aperengine.Prefabs
{
    public class Quad : Model
    {
        public Quad(float x, float y, float width, float height, Texture tex = null)
        {
            filepath = "";
            this.DrawMode = Aperengine.DrawMode.Triangles;
            vbufs = new VertexBuffer[1];
            vbufs[0] = new VertexBuffer();

            Materials = new Material[1];

            vbufs[0].SetIndices(new ushort[] { 3, 2, 0, 0, 2, 1 });
            vbufs[0].SetUVs(new float[] { 
                0,1,
                1,1,
                1,0,
                0,0
            });

            vbufs[0].SetVertices(new float[]{
                x, 0, y + height,
                x + width, 0, y + height,
                x + width, 0, y,
                x, 0, y
            });
            Materials[0] = new Material { Diffuse = tex };

            World = Matrix4.Identity;
        }

    }
}
