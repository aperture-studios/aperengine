﻿using Aperengine.Shading;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aperengine.Prefabs
{
    public class FullScreenQuad : Model
    {
        public FullScreenQuad()
        {
            vbufs = new VertexBuffer[1];
            vbufs[0] = new VertexBuffer();

            this.DrawMode = Aperengine.DrawMode.Triangles;

            vbufs[0].SetIndices(new ushort[] { 3, 2, 0, 0, 2, 1 });
            vbufs[0].SetUVs(new float[] { 
                0,1,
                1,1,
                1,0,
                0,0
            });

            vbufs[0].SetVertices(new float[]{
                -1, 1, 0,
                1, 1, 0,
                1, -1,0,
                -1, -1,0
            });

            Materials = new Material[1];

            Materials[0].Shader = new ShaderProgram(new VertexShader("Shaders/FrameBuffer"), new FragmentShader("Shaders/Default"));
        }
    }
}
