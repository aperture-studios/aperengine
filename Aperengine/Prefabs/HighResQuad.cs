﻿using Aperengine.Math;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aperengine.Prefabs
{
    public class HighResQuad : Model
    {
        public HighResQuad(float x0, float y0, int terrainWidth, int terrainHeight, Texture tex = null)
        {
            filepath = "";
            vbufs = new VertexBuffer[1];
            vbufs[0] = new VertexBuffer();

            Materials = new Material[1];

            //float[] vertices = new float[(int)((width + 1) * (height + 1) * 3)];
            //float[] uvs = new float[(int)((width + 1) * (height + 1) * 2)];
            //float[] indexCount = new float[(int)(width * height * 6)];

            //List<float> vertices = new List<float>();
            //List<float> uvs = new List<float>();
            //List<int> indices = new List<int>();

            // center x,z on origin
            //int i = 0;
            //int u = 0;
            //int v = 0;

            //for (int row = 0; row <= height; row++)
            //{
            //    for (int col = 0; col <= width; col++)
            //    {
            //        vertices[i++] = (float)col;
            //        vertices[i++] = 0.0f;
            //        vertices[i++] = (float)row;

            //        uvs[u++] = (float)(col / width);
            //        uvs[u++] = (float)(row / height);
            //    }
            //}

            //for (int row = 0; row < height; row++)
            //{
            //    for (int col = 0; col < width; col++)
            //    {
            //        indexCount[v++] = col + 1;
            //        indexCount[v++] = ((row + 1) * (width + 1)) + col + 1;
            //        indexCount[v++] = ((row + 1) * (width + 1)) + col;
            //        indexCount[v++] = ((row + 1) * (width + 1)) + col;
            //        indexCount[v++] = ((row + 1) * (width + 1)) + col + 1;
            //        indexCount[v++] = col;
            //    }
            //}
            ////vbufs[0].SetIndices(new ushort[] { 0, 1, 5, 1, 6, 5, 1, 2, 6, 2, 7, 6 });*/
            ////vbufs[0].SetIndices(indices.ToArray());
            ////vbufs[0].SetUVs(uvs.ToArray());
            ////vbufs[0].SetVertices(vertices.ToArray());

            Vector3[] vertices = new Vector3[terrainWidth * terrainHeight];
            Vector2[] uvs = new Vector2[terrainWidth * terrainHeight];
            for (int x = 0; x < terrainWidth; x++)
            {
                for (int y = 0; y < terrainHeight; y++)
                {
                    //vertices.AddRange(new float[] { x, 0, -y });
                    vertices[x + y * terrainWidth] = new Vector3(x, 0, -y);

                    uvs[(x + y * terrainWidth)] = new Vector2((float)x/(float)terrainWidth, (float)y/(float)terrainHeight);
                }
            }


            uint[] indices = new uint[(terrainWidth - 1) * (terrainHeight - 1) * 6];
            int counter = 0;
            for (int y = 0; y < terrainHeight - 1; y++)
            {
                for (int x = 0; x < terrainWidth - 1; x++)
                {
                    int lowerLeft = x + y * terrainWidth;
                    int lowerRight = (x + 1) + y * terrainWidth;
                    int topLeft = x + (y + 1) * terrainWidth;
                    int topRight = (x + 1) + (y + 1) * terrainWidth;

                    indices[counter++] = (uint)topLeft;
                    indices[counter++] = (uint)lowerRight;
                    indices[counter++] = (uint)lowerLeft;

                    indices[counter++] = (uint)topLeft;
                    indices[counter++] = (uint)topRight;
                    indices[counter++] = (uint)lowerRight;
                }
            }

            List<float> verts = new List<float>();
            List<float> uv = new List<float>();
            List<float> norms = new List<float>();
            for (int i = 0; i < vertices.Length; i++)
            {
                verts.AddRange(new float[] { vertices[i].X, vertices[i].Y, vertices[i].Z });
                uv.AddRange(new float[] { uvs[i].X, uvs[i].Y });
                norms.AddRange(new float[] { 0, 1, 0 });
            }

            vbufs[0].SetIndices(indices);
            vbufs[0].SetUVs(uv.ToArray());
            vbufs[0].SetVertices(verts.ToArray());
            vbufs[0].SetNormals(norms.ToArray());
            vbufs[0].DrawMode = DrawMode.Triangles;
            Materials[0] = new Material { Diffuse = tex };


            World = Matrix4.Identity;
        }

    }
}
