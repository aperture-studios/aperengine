﻿using OpenTK.Graphics.OpenGL4;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aperengine.Shading
{
    public class TessellationShader : Shader
    {
        internal TessellationEvalShader eval;
        internal TessellationControlShader control;

        public void SetPatchSize(int num)
        {
            GL.PatchParameter(PatchParameterInt.PatchVertices, num);
        }

        public TessellationShader(string controlShader, string evalShader)
        {
            base.shaderType = ShaderTypes.TessellationControl;
            eval = new TessellationEvalShader(evalShader);
            control = new TessellationControlShader(controlShader);
            SetPatchSize(3);
        }

        public TessellationShader(string name) : this(name, name) { }
    }

    class TessellationControlShader : Shader
    {
        private static Dictionary<string, int> tcsShaderDB = new Dictionary<string, int>();

        public TessellationControlShader(string fshader)
        {
            int result;
            base.shaderType = ShaderTypes.TessellationControl;
            if (!tcsShaderDB.ContainsKey(fshader + "/tessControl.glsl"))
            {
                id = GL.CreateShader(ShaderType.TessControlShader);
                fshader += "/tessControl.glsl";

                string file = "#version 430 core \n " + File.ReadAllText(fshader);

                GL.ShaderSource(id, file);
                GL.CompileShader(id);
                GL.GetShader(id, ShaderParameter.CompileStatus, out result);
                Console.WriteLine(fshader + "\n Tessellation Control Shader Compilation Result: " + result.ToString("X8") + "\n");
                if (result != 1) throw new Exception(GL.GetShaderInfoLog(id));

                
                tcsShaderDB.Add(fshader, base.id);
            }
            else
            {
                fshader += "/tessControl.glsl";
                base.id = tcsShaderDB[fshader];
            }
        }

    }

    class TessellationEvalShader : Shader
    {
        private static Dictionary<string, int> tesShaderDB = new Dictionary<string, int>();

        public TessellationEvalShader(string fshader)
        {
            int result;
            base.shaderType = ShaderTypes.TessellationEval;
            if (!tesShaderDB.ContainsKey(fshader + "/tessEval.glsl"))
            {
                id = GL.CreateShader(ShaderType.TessEvaluationShader);
                fshader += "/tessEval.glsl";

                string file = "#version 430 core \n " + File.ReadAllText(fshader);

                GL.ShaderSource(id, file);
                GL.CompileShader(id);
                GL.GetShader(id, ShaderParameter.CompileStatus, out result);
                Console.WriteLine(fshader + "\n Tessellation Evaluation Shader Compilation Result: " + result.ToString("X8") + "\n");
                if (result != 1) throw new Exception(GL.GetShaderInfoLog(id));

                tesShaderDB.Add(fshader, base.id);
            }
            else
            {
                fshader += "/tessEval.glsl";
                base.id = tesShaderDB[fshader];
            }
        }

    }
}
