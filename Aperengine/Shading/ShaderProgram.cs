﻿using Aperengine.Math;
using OpenTK.Graphics.OpenGL4;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aperengine.Shading
{
    public class ShaderProgram : IDisposable
    {
        private static Dictionary<string, int> programDB = new Dictionary<string, int>();
        private string shaderName;
        private Shader[] shaderStages;
        private int programId;

        public ShaderProgram(params Shader[] shaders)
        {
            variables = new Dictionary<string, shaderVars>();
            shaderStages = new Shader[5];
            programId = GL.CreateProgram();
            shaderName = "";

            foreach (Shader s in shaders)
            {
                this.AttachShader(s);
            }
        }

        public void AttachShader(Shader s)
        {

            if (s.GetShaderType() != ShaderTypes.TessellationControl) shaderStages[(int)s.GetShaderType()] = s;
            else
            {
                var shad = s as TessellationShader;
                shaderStages[(int)ShaderTypes.TessellationControl] = shad.control;
                shaderStages[(int)ShaderTypes.TessellationEval] = shad.eval;
            }

            //Build the shader name
            string name = "";
            for (int i = 0; i < 5; i++)
            {
                if (shaderStages[i] != null) name += shaderStages[i].GetID().ToString() + ",";
                else name += "-1,";
            }


            if (!programDB.ContainsKey(name))
            {
                if (!programDB.ContainsKey(shaderName)) //Backup current program
                {
                    programDB.Add(name, programId);     //If the programDB doesn't contain the key, add the program
                }

                //Recreate the current program with all its shaders
                //Link the program
                programId = GL.CreateProgram();

                for (int i = 0; i < 5; i++)
                {
                    if (shaderStages[i] != null) GL.AttachShader(programId, shaderStages[i].GetID());
                }
                int result = 1;
                GL.LinkProgram(programId);
                GL.GetProgram(programId, GetProgramParameterName.LinkStatus, out result);
                Console.WriteLine("Program Linking Result: " + result.ToString("X8") + "\n" + GL.GetProgramInfoLog(programId));

            }
            else programId = programDB[name];                                       //Retrieve the shader if it already exists

            shaderName = name;  //Finally update the shader's name
        }

        #region Shader Handler
        private enum VarType
        {
            Matrix4, Matrix3, Matrix2, Vector4, Vector3, Vector2, Float, Texture
        }
        private struct shaderVars
        {
            public int metadata;
            public int pos;
            public object obj;
            public VarType type;
        }
        //Using a dictionary with the name as the key helps prevent unnecessary entries in the variables list, old entries get overwritten automatically
        private Dictionary<string, shaderVars> variables;
        /// <summary>
        /// Set the shader and its parameters, should be called right before rendering to prevent any bugs
        /// </summary>
        /// <param name="context"></param>
        public virtual void Apply(GraphicsContext context)
        {
            GL.UseProgram(programId);

            var variables = this.variables.Values.ToList();

            for (int i = 0; i < variables.Count; i++)
            {
                switch (variables[i].type)
                {
                    case VarType.Float:
                        GL.Uniform1(variables[i].pos, (float)variables[i].obj);
                        break;
                    case VarType.Matrix2:
                        var mat2 = (Matrix2)variables[i].obj;
                        GL.UniformMatrix2(variables[i].pos, 1, false, (float[])mat2);
                        break;
                    case VarType.Matrix3:
                        var mat3 = (Matrix3)variables[i].obj;
                        GL.UniformMatrix3(variables[i].pos, 1, false, (float[])mat3);
                        break;
                    case VarType.Matrix4:
                        var mat4 = (Matrix4)variables[i].obj;
                        GL.UniformMatrix4(variables[i].pos, 1, false, (float[])mat4);
                        break;
                    case VarType.Texture:
                        (variables[i].obj as Texture).Bind(variables[i].metadata);
                        GL.Uniform1(variables[i].pos, variables[i].metadata);
                        break;
                    case VarType.Vector2:
                        Vector2 vec2 = (Vector2)variables[i].obj;
                        GL.Uniform2(variables[i].pos, vec2.X, vec2.Y);
                        break;
                    case VarType.Vector3:
                        var vec3 = (Vector3)variables[i].obj;
                        GL.Uniform3(variables[i].pos, vec3.X, vec3.Y, vec3.Z);
                        break;
                    case VarType.Vector4:
                        var vec4 = (Vector4)variables[i].obj;
                        GL.Uniform4(variables[i].pos, vec4.X, vec4.Y, vec4.Z, vec4.W);
                        break;
                }
            }
        }

        /// <summary>
        /// Clean up after the shader has been used (Apply + Draw)
        /// </summary>
        /// <param name="context">The current GraphicsContext</param>
        public virtual void Cleanup(GraphicsContext context)
        {
            var variables = this.variables.Values.ToList();

            for (int i = 0; i < variables.Count; i++)
            {
                switch (variables[i].type)
                {
                    case VarType.Texture:
                        Texture.UnBind(variables[i].metadata);
                        break;
                    default:
                        break;
                }
            }

            GL.UseProgram(0);
        }
        #endregion

        #region Shader Variables
        public void SetShaderBool(string name, bool val)
        {
            SetShaderFloat(name, val ? 1 : 0);
        }
        public void SetShaderMatrix(string name, Matrix4 val)
        {
            variables[name] = new shaderVars()
            {
                obj = val,
                pos = GL.GetUniformLocation(programId, name),
                type = VarType.Matrix4
            };
        }

        public void SetShaderMatrix(string name, Matrix2 val)
        {
            variables[name] = new shaderVars()
            {
                obj = val,
                pos = GL.GetUniformLocation(programId, name),
                type = VarType.Matrix2
            };
        }

        public void SetShaderMatrix(string name, Matrix3 val)
        {
            variables[name] = new shaderVars()
            {
                obj = val,
                pos = GL.GetUniformLocation(programId, name),
                type = VarType.Matrix3
            };
        }

        public void SetShaderVector(string name, Vector4 val)
        {
            variables[name] = new shaderVars()
            {
                obj = val,
                pos = GL.GetUniformLocation(programId, name),
                type = VarType.Vector4
            };
        }

        public void SetShaderVector(string name, Vector3 val)
        {
            variables[name] = new shaderVars()
            {
                obj = val,
                pos = GL.GetUniformLocation(programId, name),
                type = VarType.Vector3
            };
        }

        public void SetShaderVector(string name, Vector2 val)
        {
            variables[name] = new shaderVars()
            {
                obj = val,
                pos = GL.GetUniformLocation(programId, name),
                type = VarType.Vector2
            };
        }

        public void SetShaderFloat(string name, float val)
        {
            variables[name] = new shaderVars()
            {
                obj = val,
                pos = GL.GetUniformLocation(programId, name),
                type = VarType.Float
            };
        }

        int texUnit = 0;
        public void SetTexture(string name, Texture tex)
        {
            if (variables.ContainsKey(name))
            {
                variables[name] = new shaderVars()
                {
                    metadata = variables[name].metadata,
                    obj = tex,
                    pos = variables[name].pos,
                    type = VarType.Texture
                };
            }
            else
            {
                variables[name] = new shaderVars()
                {
                    metadata = texUnit++,
                    obj = tex,
                    pos = GL.GetUniformLocation(programId, name),
                    type = VarType.Texture
                };
            }
        }
        #endregion


        public void Dispose()
        {
            // throw new NotImplementedException();
        }
    }
}
