﻿using Aperengine.Math;
using OpenTK.Graphics.OpenGL4;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aperengine.Shading
{
    public enum ShaderTypes
    {
        Vertex = 0, Fragment = 4, Geometry = 3, TessellationControl = 1, TessellationEval = 2
    }

    /// <summary>
    /// A Shader Program Object
    /// </summary>
    public class Shader : IDisposable
    {
        public Shader()
        {
            variables = new Dictionary<string, shaderVars>();
        }

        protected ShaderTypes shaderType;
        protected int id;
        internal int GetID()
        {
            return id;
        }

        internal ShaderTypes GetShaderType()
        {
            return shaderType;
        }

        #region Shader Handler
        private enum VarType
        {
            Matrix4, Matrix3, Matrix2, Vector4, Vector3, Vector2, Float, Texture
        }
        private struct shaderVars
        {
            public int metadata;
            public int pos;
            public object obj;
            public VarType type;
        }
        //Using a dictionary with the name as the key helps prevent unnecessary entries in the variables list, old entries get overwritten automatically
        private Dictionary<string, shaderVars> variables;
        /// <summary>
        /// Set the shader and its parameters, should be called right before rendering to prevent any bugs
        /// </summary>
        /// <param name="context"></param>
        internal virtual void Apply(GraphicsContext context)
        {
            var variables = this.variables.Values.ToList();

            for (int i = 0; i < variables.Count; i++)
            {
                switch (variables[i].type)
                {
                    case VarType.Float:
                        GL.ProgramUniform1(id, variables[i].pos, (float)variables[i].obj);
                        break;
                    case VarType.Matrix2:
                        var mat2 = (Matrix2)variables[i].obj;
                        GL.ProgramUniformMatrix2(id, variables[i].pos, 1, false, (float[])mat2);
                        break;
                    case VarType.Matrix3:
                        var mat3 = (Matrix3)variables[i].obj;
                        GL.ProgramUniformMatrix3(id, variables[i].pos, 1, false, (float[])mat3);
                        break;
                    case VarType.Matrix4:
                        var mat4 = (Matrix4)variables[i].obj;
                        GL.ProgramUniformMatrix4(id, variables[i].pos, 1, false, (float[])mat4);
                        break;
                    case VarType.Texture:
                        (variables[i].obj as Texture).Bind(variables[i].metadata);
                        GL.ProgramUniform1(id, variables[i].pos, variables[i].metadata);
                        break;
                    case VarType.Vector2:
                        Vector2 tmpA = (Vector2)variables[i].obj;
                        GL.ProgramUniform2(id, variables[i].pos, 1, new float[] { tmpA.X, tmpA.Y });
                        break;
                    case VarType.Vector3:
                        Vector3 tmpB = (Vector3)variables[i].obj;
                        GL.ProgramUniform3(id, variables[i].pos, 1, new float[] { tmpB.X, tmpB.Y, tmpB.Z });
                        break;
                    case VarType.Vector4:
                        Vector4 tmpC = (Vector4)variables[i].obj;
                        GL.ProgramUniform4(id, variables[i].pos, 1, new float[] { tmpC.X, tmpC.Y, tmpC.Z, tmpC.W });
                        break;
                }
            }

        }

        /// <summary>
        /// Clean up after the shader has been used (Apply + Draw)
        /// </summary>
        /// <param name="context">The current GraphicsContext</param>
        internal virtual void Cleanup(GraphicsContext context)
        {
            var variables = this.variables.Values.ToList();

            for (int i = 0; i < variables.Count; i++)
            {
                switch (variables[i].type)
                {
                    case VarType.Texture:
                        Texture.UnBind(variables[i].metadata);
                        break;
                    default:
                        break;
                }
            }
        }
        #endregion

        #region Shader Variables
        public void SetShaderBool(string name, bool val)
        {
            SetShaderFloat(name, val ? 1 : 0);
        }
        public void SetShaderMatrix(string name, Matrix4 val)
        {
            variables[name] = new shaderVars()
            {
                obj = val,
                pos = GL.GetUniformLocation(this.id, name),
                type = VarType.Matrix4
            };
        }

        public void SetShaderMatrix(string name, Matrix2 val)
        {
            variables[name] = new shaderVars()
            {
                obj = val,
                pos = GL.GetUniformLocation(this.id, name),
                type = VarType.Matrix2
            };
        }

        public void SetShaderMatrix(string name, Matrix3 val)
        {
            variables[name] = new shaderVars()
            {
                obj = val,
                pos = GL.GetUniformLocation(this.id, name),
                type = VarType.Matrix3
            };
        }

        public void SetShaderVector(string name, Vector4 val)
        {
            variables[name] = new shaderVars()
            {
                obj = val,
                pos = GL.GetUniformLocation(this.id, name),
                type = VarType.Vector4
            };
        }

        public void SetShaderVector(string name, Vector3 val)
        {
            variables[name] = new shaderVars()
            {
                obj = val,
                pos = GL.GetUniformLocation(this.id, name),
                type = VarType.Vector3
            };
        }

        public void SetShaderVector(string name, Vector2 val)
        {
            variables[name] = new shaderVars()
            {
                obj = val,
                pos = GL.GetUniformLocation(this.id, name),
                type = VarType.Vector2
            };
        }

        public void SetShaderFloat(string name, float val)
        {
            variables[name] = new shaderVars()
            {
                obj = val,
                pos = GL.GetUniformLocation(this.id, name),
                type = VarType.Float
            };
        }

        int texUnit = 0;
        public void SetTexture(string name, Texture tex)
        {
            if (variables.ContainsKey(name))
            {
                variables[name] = new shaderVars()
                {
                    metadata = variables[name].metadata,
                    obj = tex,
                    pos = variables[name].pos,
                    type = VarType.Texture
                };
            }
            else
            {
                variables[name] = new shaderVars()
                {
                    metadata = texUnit++,
                    obj = tex,
                    pos = GL.GetUniformLocation(this.id, name),
                    type = VarType.Texture
                };
            }
        }
        #endregion


        public void Dispose()
        {
        }

    }
}
