﻿using OpenTK.Graphics.OpenGL4;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aperengine.Shading
{
    public class VertexShader : Shader
    {
        private static Dictionary<string, int> vshaderDB = new Dictionary<string, int>();

        public VertexShader(string fshader)
        {
            int result;
            base.shaderType = ShaderTypes.Vertex;
            if (!vshaderDB.ContainsKey(fshader + "/vertex.glsl"))
            {
                id = GL.CreateShader(ShaderType.VertexShader);
                fshader += "/vertex.glsl";

                string file = "#version 430 core \n " + File.ReadAllText(fshader);

                GL.ShaderSource(id, file);
                GL.CompileShader(id);
                GL.GetShader(id, ShaderParameter.CompileStatus, out result);
                Console.WriteLine(fshader + "\n Vertex Shader Compilation Result: " + result.ToString("X8") + "\n");
                if (result != 1) throw new Exception(GL.GetShaderInfoLog(id));

                vshaderDB.Add(fshader, base.id);
            }
            else
            {
                fshader += "/vertex.glsl";
                base.id = vshaderDB[fshader];
            }
        }
    }
}
