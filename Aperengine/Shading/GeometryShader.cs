﻿using OpenTK.Graphics.OpenGL4;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aperengine.Shading
{
    public class GeometryShader : Shader
    {
        private static Dictionary<string, int> gshaderDB = new Dictionary<string, int>();

        public GeometryShader(string fshader)
        {
            int result;
            base.shaderType = ShaderTypes.Geometry;
            if (!gshaderDB.ContainsKey(fshader + "/geometry.glsl"))
            {
                id = GL.CreateShader(ShaderType.GeometryShader);
                fshader += "/geometry.glsl";

                GL.ShaderSource(id, File.ReadAllText(fshader));
                GL.CompileShader(id);
                GL.GetShader(id, ShaderParameter.CompileStatus, out result);
                Console.WriteLine(fshader + "\n Geometry Shader Compilation Result: " + result.ToString("X8") + "\n");
                if (result != 1) throw new Exception(GL.GetShaderInfoLog(id));

                gshaderDB.Add(fshader, base.id);
            }
            else
            {
                fshader += "/geometry.glsl";
                base.id = gshaderDB[fshader];
            }
        }

    }
}
