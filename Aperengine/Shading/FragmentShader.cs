﻿using OpenTK.Graphics.OpenGL4;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aperengine.Shading
{
    public class FragmentShader : Shader
    {
        private static Dictionary<string, int> fshaderDB = new Dictionary<string, int>();

        public FragmentShader(string fshader)
        {
            int result;
            base.shaderType = ShaderTypes.Fragment;
            if (!fshaderDB.ContainsKey(fshader + "/fragment.glsl"))
            {
                id = GL.CreateShader(ShaderType.FragmentShader);
                fshader += "/fragment.glsl";

                string file = "#version 430 core \n " + File.ReadAllText(fshader);

                GL.ShaderSource(id, file);
                GL.CompileShader(id);
                GL.GetShader(id, ShaderParameter.CompileStatus, out result);
                Console.WriteLine(fshader + "\n Fragment Shader Compilation Result: " + result.ToString("X8") + "\n");
                if (result != 1) throw new Exception(GL.GetShaderInfoLog(id));

                
                fshaderDB.Add(fshader, base.id);
            }
            else
            {
                fshader += "/fragment.glsl";
                base.id = fshaderDB[fshader];
            }
        }
    }
}
