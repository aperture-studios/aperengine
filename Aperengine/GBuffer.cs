﻿using OpenTK;
using OpenTK.Graphics.OpenGL4;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aperengine
{
    /// <summary>
    /// A special Framebuffer which supports Multiple Render Targets
    /// </summary>
    public class GBuffer : IDisposable
    {
        private FrameBuffer Buffer;
        public Dictionary<string, Tuple<FrameBufferTexture, int>> RenderTargets { get; private set; }
        private DrawBuffersEnum[] attachments;

        public int Width
        {
            get
            {
                return Buffer.Width;
            }
        }

        public int Height
        {
            get
            {
                return Buffer.Height;
            }
        }

        public Vector4 Viewport
        {
            get
            {
                return Buffer.Viewport;
            }
            set
            {
                Buffer.Viewport = value;
            }
        }

        public GBuffer(int width, int height, PixelInternalFormat pf = PixelInternalFormat.Rgba)
        {
            Buffer = new FrameBuffer(width, height, pf);
            RenderTargets = new Dictionary<string, Tuple<FrameBufferTexture, int>>();
            attachments = new DrawBuffersEnum[] { };
            this.Add("Color0", 0, Buffer.ColorTarget);
        }

        public void Add(string name, int attachmentNumber, FrameBufferTexture tex)
        {
            //Backup current framebuffer
            var tmp = FrameBuffer.GetCurrentFrameBuffer();

            //Add texture to convenience dictionary
            RenderTargets.Add(name, new Tuple<FrameBufferTexture, int>(tex, attachmentNumber));

            //Bind the GBuffer
            Buffer.Bind();
            //Generate the appropriate bind value
            tex.BindToFrameBuffer((int)FramebufferAttachment.ColorAttachment0 + attachmentNumber);

            var temp = new List<DrawBuffersEnum>(attachments);
            temp.Add(DrawBuffersEnum.ColorAttachment0 + attachmentNumber);
            attachments = temp.OrderBy(x => x).ToArray();

            //Restore original framebuffer
            if (tmp != null) tmp.Bind();
            else GL.BindFramebuffer(FramebufferTarget.Framebuffer, 0);
        }

        public FrameBufferTexture this[string key]
        {
            get
            {
                return RenderTargets[key].Item1;
            }
        }

        public void Bind()
        {
            //All the new textures have already been bound so we only need to bind the buffer itself
            Buffer.Bind();
            GL.DrawBuffers(attachments.Length, attachments);
        }

        /// <summary>
        /// Dispose the GBuffer, this does not dispose the bound textures
        /// </summary>
        public void Dispose(bool disposeTex)
        {
            if (disposeTex)
            {
                foreach (string key in RenderTargets.Keys)
                {
                    RenderTargets[key].Item1.Dispose(); //Dispose all the bound textures
                }
            }
            Buffer.Dispose(disposeTex);
        }



        public void Dispose()
        {
            Dispose(false);
        }
    }
}
