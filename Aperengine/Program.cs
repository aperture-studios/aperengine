﻿using Aperengine.Prefabs;
using Aperengine.Shading;
using Aperengine.Math;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aperengine
{
    class Program
    {
        static GraphicsContext Context;
        static Model fsq, sphere, ground;
        static FirstPersonCameraLL Camera;
        static Sky sky;

        static void Main(string[] args)
        {
            Context = new GraphicsContext(1280, 720, 1, false);
            Context.Render += Render;

            Context.Camera = new FirstPersonCameraLL(new Vector3(1560.857f, 420.7243f, -596.5911f), new Vector3(0, 1, 0f));

            Context.ZFar = 5000f;
            //Context.Update += Camera.Update;

            sky = new Sky(new Vector3(35), new Vector3(200), 1);
            Context.Update += sky.Update;

            sphere = new Sphere(1f, 20);
            sphere.World = Matrix4.CreateTranslation(0, 5, -1);
            sphere.Materials[0].Shader = new ShaderProgram(new VertexShader("Shaders/GBuffer"), new FragmentShader("Shaders/SkyDome"));
            sphere.Materials[0].Diffuse = new Texture("Shaders/SkyDome/sky.jpg");
            sphere.Materials[0].Lit = 0;

            Context.Lights.Add(new DirectionalLight()
            {
                Color = Vector3.Zero,
                Direction = new Vector3(1, -1f, 0)
            });

            fsq = new HighResQuad(0, 0, 500, 500);
            fsq.Materials[0].Shader = new ShaderProgram(new VertexShader("Shaders/HeightMap"), new FragmentShader("Shaders/HeightMap"));
            fsq.Materials[0].Shader.SetTexture("heightMap", new Texture("Resources/sibenik/terrain-heightmap.jpg"));
            //fsq.DrawMode = DrawMode.Patches;
            fsq.DrawMode = DrawMode.Triangles;
            //fsq = Model.Load("Resources/sibenik/sibenik.obj");
            //fsq = Model.Load("Resources/city/city.obj");
            //ground = Model.Load("Resources/city/ground.obj");
            //fsq.Materials[0] = Material.Load("Resources/ShipA/Materials/ShipA.mat");
            for (int i = 0; i < fsq.Materials.Length; i++)
            {
                fsq.Materials[i].Lit = 1f;
                fsq.Materials[i].FresnelTerm = 0.8f;
                fsq.Materials[i].DiffuseReflectivity = 0.3f;
                fsq.Materials[i].Reflectivity = 1f;
                fsq.Materials[i].Diffuse = new Texture("Resources/sibenik/ground.png");
                fsq.Materials[i].NormalMap = new Texture("Resources/sibenik/kamen-normals.png");
            }


            //ground.Materials[0].Lit = 0.1f;
            //ground.Materials[0].FresnelTerm = 0.1f;
            //ground.Materials[0].DiffuseReflectivity = 0.8f;
            //ground.Materials[0].Reflectivity = 1f;
            //ground.Materials[0].NormalMap = new Texture("Resources/sibenik/kamen-normals.png");
            //fsq.Materials[1].Reflectivity = 0;
            //fsq.Materials[52].Reflectivity = 0f;
            Context.SSAO = true;
            Context.Lighting = true;
            Context.MSAALevel = 1;
            Context.Start();
        }

        static float y = 0;
        static void Render(GraphicsContext Context)
        {
            Context.Clear(0f, 0, 0f, 0);
            sky.Draw(Context);
            //Context.Wireframe(true);
            Context.EnableDepthTest();
            //Context.EnableCullFace();
            //Context.Transparency(true);


            Context.Title = Context.FPS.ToString();
            fsq.World = Matrix4.Scale(20f, 22f, 20f) * Matrix4.CreateTranslation(-500, 0, -500);
            fsq.Draw(Context);
            y += 0.001f;
            //Context.AmbientLight = (float)(System.Math.Cos(y) * MathHelper.E);
            //if (Context.AmbientLight > 1) Context.AmbientLight = 0;
            //sphere.Materials[0].Shader.SetShaderFloat("time", y);

            Context.Transparency(false);
            Context.DisableCullFace();
            Context.DisableDepthTest();
            Context.Wireframe(false);

        }

    }
}
