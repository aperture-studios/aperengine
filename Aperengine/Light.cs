﻿using Aperengine.Math;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aperengine
{
    public class Light
    {
        public Vector3 Color { get; set; }
        public bool Visible { get; set; }
        public bool CastShadow { get; set; }
    }

    public class DirectionalLight : Light
    {
        public Vector3 Direction { get; set; }
    }

    public class PointLight : Light
    {
        public Vector3 Position { get; set; }
        public float Intensity { get; set; }
    }
}
