﻿using OpenTK;
using OpenTK.Graphics.OpenGL4;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aperengine
{
    public class FrameBuffer : IDisposable
    {
        private int fbufId = 0, width, height;

        public int Width
        {
            get
            {
                return width;
            }
        }
        public int Height
        {
            get
            {
                return height;
            }
        }
        public FrameBufferTexture ColorTarget { get; set; }
        public FrameBufferTexture DepthTarget { get; set; }
        public Vector4 Viewport { get; set; }

        public FrameBuffer(int width, int height, PixelInternalFormat pf = PixelInternalFormat.Rgba)
        {
            this.width = width;
            this.height = height;
            Viewport = new Vector4(0, 0, width, height);

            fbufId = GL.GenFramebuffer();
            GL.BindFramebuffer(FramebufferTarget.Framebuffer, fbufId);

            ColorTarget = new FrameBufferTexture(width, height, pf, PixelFormat.Bgra, PixelType.HalfFloat);
            DepthTarget = new FrameBufferTexture(width, height, PixelInternalFormat.DepthComponent32, PixelFormat.DepthComponent, PixelType.Float);

            ColorTarget.BindToFrameBuffer((int)FramebufferAttachment.ColorAttachment0);
            DepthTarget.BindToFrameBuffer((int)FramebufferAttachment.DepthAttachment);

            GL.DrawBuffers(1, new DrawBuffersEnum[] { DrawBuffersEnum.ColorAttachment0});

            if (GL.CheckFramebufferStatus(FramebufferTarget.Framebuffer) != FramebufferErrorCode.FramebufferComplete) throw new Exception(GL.CheckFramebufferStatus(FramebufferTarget.Framebuffer).ToString());

            GL.BindFramebuffer(FramebufferTarget.Framebuffer, 0);
        }

        public void Bind()
        {
            if (fbufId != -1)
            {
                GL.BindFramebuffer(FramebufferTarget.Framebuffer, fbufId);
                GL.Viewport((int)Viewport.X, (int)Viewport.Y, (int)Viewport.Z, (int)Viewport.W);
                curFBuf = this;
                GL.DrawBuffers(1, new DrawBuffersEnum[] { DrawBuffersEnum.ColorAttachment0 });
            }
        }

        /// <summary>
        /// Dispose the Framebuffer and the bound textures
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
        }

        /// <summary>
        /// Dispose the Framebuffer
        /// </summary>
        /// <param name="disposeImgs">If true, disposes the bound images as well</param>
        public void Dispose(bool disposeImgs)
        {
            GL.DeleteFramebuffer(fbufId);
            fbufId = -1;
            if (disposeImgs)
            {
                ColorTarget.Dispose();
                DepthTarget.Dispose();
            }
        }

        private static FrameBuffer curFBuf;
        public static FrameBuffer GetCurrentFrameBuffer()
        {
            return curFBuf;
        }
    }
}
