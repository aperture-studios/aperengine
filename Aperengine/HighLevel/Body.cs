﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BEPUphysics.Entities;
using System.IO;
using Aperengine.Math;
using BEPUphysics.Entities.Prefabs;
using Assimp;

namespace Aperengine.HighLevel
{
    /// <summary>
    /// A body is a combination of Models with collision data and memory saving optimizations, essentially a higher level compound model implementation
    /// </summary>
    public class Body
    {
        public string Name { get; set; }
        public Model[] Models { get; set; }
        public Entity PhysicsBody { get; set; }
        public Dictionary<string, string> Properties { get; set; }  //TODO Make this update its bound World object with its properties so the world object knows to call the appropriate handler
        public World Parent { get; set; }
        public Matrix4 World { get; set; }

        /// <summary>
        /// Constructs a body from models
        /// </summary>
        /// <param name="models">The models to construct the body from</param>
        public Body(params Model[] models)
        {
            Models = models;
            Properties = new Dictionary<string, string>();
        }

        public virtual void Draw(GraphicsContext Context)
        {
            //TODO Update All meshes world matrices when the world matrix is updated
            for (int i = 0; i < Models.Length; i++)
            {
                Models[i].Draw(Context);
            }
        }

        /// <summary>
        /// Load a body from a file
        /// </summary>
        /// <param name="filename">The file to load the body from</param>
        /// <returns>The loaded body</returns>
        public static Body Load(string filename)
        {
            List<Model> models = new List<Model>();
            string[] lines = File.ReadAllLines(filename);
            Body bdy = new Body();
            bool propertiesMode = false;
            float mass = 0;
            bdy.Name = lines[0].Replace("[Name]", "").Trim();
            for (int i = 1; i < lines.Length; i++)
            {
                switch (lines[i].Split(']')[0])
                {
                    case "[Mesh":
                        models.Add(Model.Load(lines[i].Split(']')[1].Trim()));
                        break;
                    case "[CollisionMesh":
                        //TODO Load mesh as collision body rather than renderable mesh
                        bdy.PhysicsBody = LoadMeshCollidable(lines[i].Split(']')[1].Trim(), mass);
                        break;
                    case "[Material":
                        int index = int.Parse(lines[i].Split(']')[1].Substring(1));
                        models[models.Count - 1].Materials[index] = Material.Load(lines[i].Split(']')[2].Trim());
                        break;
                    case "[Position":
                        float x = float.Parse(lines[i].Split('{')[1].Split(',')[0]);
                        float y = float.Parse(lines[i].Split('{')[1].Split(',')[1]);
                        float z = float.Parse(lines[i].Split('{')[1].Split(',')[2].TrimEnd('}'));
                        models[models.Count - 1].World = Matrix4.CreateTranslation(x, y, z);
                        break;
                    case "[Rotation":
                        //TODO Multiply rotation matrix
                        break;
                    case "[Properties":
                        propertiesMode = true;
                        break;
                    case "[Mass":
                        mass = float.Parse(lines[i].Split(']')[1]);
                        break;
                    default:
                        if (propertiesMode)
                        {
                            string key = lines[i].Split(']')[0].Substring(1);
                            string value = lines[i].Split(']')[1].Trim();
                            bdy.Properties.Add(key, value);
                        }
                        break;
                }
            }
            return bdy;
        }

        private static MobileMesh LoadMeshCollidable(string filename, float mass)
        {
            AssimpContext context = new AssimpContext();
            Scene model = context.ImportFile(filename);

            string baseDir = Path.GetDirectoryName(filename);

                Mesh m = model.Meshes[0];

                BEPUutilities.Vector3[] vertices = new BEPUutilities.Vector3[m.VertexCount];
                for (int v = 0; v < m.VertexCount ; v ++)
                {
                    vertices[v] = new BEPUutilities.Vector3(m.Vertices[v].X, m.Vertices[v].Y, m.Vertices[v].Z);
                }

                int[] indices = null;
                if (m.GetUnsignedIndices() != null)
                {
                    indices = Array.ConvertAll(m.GetUnsignedIndices(), s => (int)s);
                }
                else
                {
                    indices = new int[m.VertexCount];
                    for (ushort n = 0; n < indices.Length; n++) indices[n] = n;
                }

                MobileMesh hull = null;
                if (mass != 0) hull = new MobileMesh(vertices, indices, BEPUutilities.AffineTransform.Identity, BEPUphysics.CollisionShapes.MobileMeshSolidity.Solid, mass);
                else hull = new MobileMesh(vertices, indices, BEPUutilities.AffineTransform.Identity, BEPUphysics.CollisionShapes.MobileMeshSolidity.Solid);
                return hull;
        }

    }
}
