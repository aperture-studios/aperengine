﻿using Aperengine.Math;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aperengine.HighLevel
{
    public class FirstPersonCamera : CameraBase
    {
        public FirstPersonCamera(Vector3 Position, Vector3 Direction)
        {
            this.LowLevel = new FirstPersonCameraLL(Position, Direction);
        }
    }
}
