﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aperengine.HighLevel
{
    /// <summary>
    /// This represents the game world. It is a collection of bodies and their initial positions.
    /// </summary>
    public class World
    {
        public List<Body> Entities { get; set; }
        public GraphicsContext Context { get; private set; }
        public CameraBase Camera { get { return _camera; } set { _camera = value; Context.Camera = _camera.LowLevel; } }

        private CameraBase _camera;

        public World(GraphicsContext Context)
        {
            Entities = new List<Body>();
            this.Context = Context;
            Context.Render = Draw;
            Context.Update = Update;
        }

        public void Add(Body b)
        {
            Entities.Add(b);
            b.Parent = this;

            //TODO register the body into the physics world
        }

        public void RegisterEventHandler(string eventName, Action<string> handler)
        {
            //Call the necessary event handlers when the events are detected
        }

        public void Start(int frameRate, int updateRate)
        {
            Context.Start(frameRate, updateRate);
        }

        private void Draw(GraphicsContext Context)
        {
            //draw the current world
            for (int i = 0; i < Entities.Count; i++)
            {
                Entities[i].Draw(Context);
            }


            //TODO implement some sort of partitioning scheme to speed up rendering and only show what's likely visible
        }

        private void Update(GraphicsContext Context)
        {
            //Update the view matrix
        }

        /// <summary>
        /// Load a world from a file
        /// </summary>
        /// <param name="filename">The file to load the world from</param>
        /// <returns>A world object of the file loaded</returns>
        public static World Load(string filename, GraphicsContext context)
        {
            World world = new World(context);
            string[] lines = File.ReadAllLines(filename);

            for (int i = 0; i < lines.Length; i++)
            {
                switch (lines[i].Split(']')[0].Trim())
                {
                    case "[Body":
                        break;
                    case "[Position":
                        break;
                    case "[Rotation":
                        break;
                }
            }

            return world;
        }
    }
}
