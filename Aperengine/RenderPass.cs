﻿using Aperengine.Prefabs;
using Aperengine.Shading;
using OpenTK.Graphics.OpenGL4;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aperengine
{
    public class RenderPass : IDisposable
    {
        public GBuffer RenderTarget;
        public ShaderProgram Shader;
        public Texture Diffuse;

        private static FullScreenQuad quad = new FullScreenQuad();

        public RenderPass(int width, int height, PixelInternalFormat pf)
        {
            RenderTarget = new GBuffer(width, height, pf);
        }

        public void Clear(GraphicsContext context, float r, float g, float b, float a)
        {
            var tmp = FrameBuffer.GetCurrentFrameBuffer();
            RenderTarget.Bind();
            context.Clear(r, g, b, a);
            tmp.Bind();
        }

        public void Apply(GraphicsContext context)
        {
            var tmp = FrameBuffer.GetCurrentFrameBuffer();
            RenderTarget.Bind();
            quad.Materials[0].Shader = Shader;
            quad.Materials[0].Diffuse = Diffuse;
            quad.Draw(context);
            tmp.Bind();
        }

        public void Dispose()
        {
            Dispose(false, false, false);
        }

        public void Dispose(bool dispRT, bool dispShader, bool dispDiff)
        {
            this.RenderTarget.Dispose(dispRT);
            if (dispShader) this.Shader.Dispose();
            if (dispDiff) this.Diffuse.Dispose();
        }
    }
}
