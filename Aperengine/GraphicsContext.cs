﻿using Aperengine.Prefabs;
using Aperengine.Shading;
using Aperengine.Math;
using OpenTK.Graphics.OpenGL4;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Aperengine
{
    public class GraphicsContext : IDisposable
    {
        #region Public Properties
        /// <summary>
        /// View Matrix
        /// </summary>
        public Matrix4 View { get; set; }
        /// <summary>
        /// Projection Matrix
        /// </summary>
        public Matrix4 Projection { get; set; }
        /// <summary>
        /// Que Actions for invocation on the main application thread
        /// </summary>
        public Queue<Action> MainThreadInvocationQueue { get; set; }
        /// <summary>
        /// Render Handler
        /// </summary>
        public Action<GraphicsContext> Render { get; set; }
        /// <summary>
        /// Update Handler
        /// </summary>
        public Action<GraphicsContext> Update { get; set; }
        /// <summary>
        /// The Title of the Game Window
        /// </summary>
        public string Title
        {
            get
            {
                return window.Title;
            }
            set
            {
                window.Title = value;
            }
        }

        public Vector2 Size
        {
            get
            {
                return new Vector2(window.ClientSize.Width, window.ClientSize.Height);
            }
            set
            {
                window.ClientSize = new System.Drawing.Size((int)value.X, (int)value.Y);
            }
        }

        public float ZNear { get; set; }
        public float ZFar { get; set; }

        /// <summary>
        /// The rendered Frames per second
        /// </summary>
        public double FPS
        {
            get
            {
                return window.RenderFrequency;
            }
        }
        /// <summary>
        /// Enable/Disable Vertical Synchronization
        /// </summary>
        public bool VSync
        {
            get
            {
                return window.VSync == OpenTK.VSyncMode.On;
            }
            set
            {
                window.VSync = (value) ? OpenTK.VSyncMode.On : OpenTK.VSyncMode.Off;
            }
        }
        /// <summary>
        /// Change the MSAA level
        /// </summary>
        public int MSAALevel
        {
            get
            {
                return aaLevel;
            }
            set
            {
                if (value == 0) value = 1;
                aaLevel = value;
                MainThreadInvocationQueue.Enqueue(() =>
                {
                    //DefaultFrameBuffer.Viewport = new Vector4(0, 0, window.ClientSize.Width * aaLevel, window.ClientSize.Height * aaLevel);
                });
            }
        }
        /// <summary>
        /// Contains all the lights
        /// </summary>
        public List<Light> Lights { get; set; }
        public bool Lighting { get; set; }
        public bool SSAO { get; set; }
        public Vector3 AmbientLight { get; set; }
        public Camera Camera { get; set; }
        #endregion

        #region Private Global Variables
        OpenTK.GameWindow window;
        GBuffer DefaultFrameBuffer;
        Model backBufferRenderer;
        VertexShader frameBufferVShader, screenSpaceVShader, lightMapVShader, ssaoVShader;
        FragmentShader blurHorizontalFShader, blurVerticalFShader, screenSpaceFShader, lightMapFShader, ssaoFShader, frameBufferFShader;
        ShaderProgram backBufferShader, lightShader, ssaoShader, horizontalBlur, verticalBlur, ssr, copyShader;
        RenderPass lightPass, ssaoPass, horizontalBlurPass, verticalBlurPass, shadowReflectionPass;
        int aaLevel;
        #endregion

        #region Contructors
        public GraphicsContext() : this(960, 540, 1, false) { }
        public GraphicsContext(int w, int h, int rtScale, bool fullscreen)
        {
            if (!fullscreen)
            {
                window = new OpenTK.GameWindow(w, h);
            }
            else
            {

                window = new OpenTK.GameWindow(Screen.PrimaryScreen.Bounds.Width, Screen.PrimaryScreen.Bounds.Height);
                window.WindowBorder = OpenTK.WindowBorder.Hidden;
                window.WindowState = OpenTK.WindowState.Fullscreen;
            }

            MainThreadInvocationQueue = new Queue<Action>();

            ZNear = 0.1f;
            ZFar = 1000f;

            Lights = new List<Light>();

            //Register render handler
            window.RenderFrame += window_RenderFrame;
            //Register the internally managed update handler
            window.UpdateFrame += window_UpdateFrame;
            //Register the internal resize handler
            window.Resize += window_Resize;
            //Register the internal keyboard handlers
            window.Keyboard.KeyDown += Keyboard_KeyDown;
            window.Keyboard.KeyUp += Keyboard_KeyUp;
            window.Mouse.ButtonDown += Mouse_ButtonDown;
            window.Mouse.ButtonUp += Mouse_ButtonUp;

            //Render cubemap, opposite faces luminances are used to calculate diffuse interreflections and color bleed, the depth is used to generate shadows by adjusting the perspective from the cubemap


            //Disable Vssync
            window.VSync = OpenTK.VSyncMode.Off;

            aaLevel = rtScale;
            int width = window.ClientSize.Width * rtScale;
            int height = window.ClientSize.Height * rtScale;

            View = Matrix4.LookAt(new Vector3(0, -1, 0), Vector3.Zero, Vector3.UnitZ);
            Projection = Matrix4.CreatePerspectiveFieldOfView(MathHelper.DegreesToRadians(45), (float)width / (float)height, ZNear, ZFar);
            Cursor.Position = new System.Drawing.Point(w / 2 + window.X, h / 2 + window.Y);
            Initialize(width, height);
        }
        #endregion

        private void Initialize(int width, int height)
        {
            #region Default Framebuffer Initialization
            //Reinitialize the framebuffer to the new resolution
            if (DefaultFrameBuffer != null) DefaultFrameBuffer.Dispose(true);
            DefaultFrameBuffer = new GBuffer(width, height, PixelInternalFormat.Rgba16f);
            DefaultFrameBuffer.Add("Normals", 3, new FrameBufferTexture(width, height, PixelInternalFormat.Rgba16f));
            DefaultFrameBuffer.Add("UVs", 2, new FrameBufferTexture(width, height, PixelInternalFormat.Rgb16f));
            DefaultFrameBuffer.Add("MaterialDataA", 4, new FrameBufferTexture(width, height, PixelInternalFormat.Rgba16f));
            DefaultFrameBuffer.Add("MaterialDataB", 5, new FrameBufferTexture(width, height, PixelInternalFormat.Rgba16f));
            DefaultFrameBuffer.Add("WorldPos", 1, new FrameBufferTexture(width, height, PixelInternalFormat.Rgba16f));
            #endregion

            #region initialize shaders
            if (frameBufferFShader == null) frameBufferFShader = new FragmentShader("Shaders/FrameBuffer");
            if (frameBufferVShader == null) frameBufferVShader = new VertexShader("Shaders/FrameBuffer");
            if (blurHorizontalFShader == null) blurHorizontalFShader = new FragmentShader("Shaders/BlurHorizontal");
            if (blurVerticalFShader == null) blurVerticalFShader = new FragmentShader("Shaders/BlurVertical");
            if (screenSpaceVShader == null) screenSpaceVShader = new VertexShader("Shaders/ScreenSpace");
            if (screenSpaceFShader == null) screenSpaceFShader = new FragmentShader("Shaders/ScreenSpace");
            if (ssaoVShader == null) ssaoVShader = new VertexShader("Shaders/SSGI");
            if (ssaoFShader == null) ssaoFShader = new FragmentShader("Shaders/SSGI");
            if (lightMapVShader == null) lightMapVShader = new VertexShader("Shaders/LightMap");
            if (lightMapFShader == null) lightMapFShader = new FragmentShader("Shaders/LightMap");

            if (horizontalBlur == null) horizontalBlur = new ShaderProgram(frameBufferVShader, blurHorizontalFShader);
            horizontalBlur.SetShaderFloat("blurSize", 0.0005f);

            if (verticalBlur == null) verticalBlur = new ShaderProgram(frameBufferVShader, blurVerticalFShader);
            verticalBlur.SetShaderFloat("blurSize", 0.0005f);

            if (backBufferShader == null) backBufferShader = new ShaderProgram(screenSpaceVShader, screenSpaceFShader);
            backBufferShader.SetTexture("normals", DefaultFrameBuffer["Normals"]);
            backBufferShader.SetTexture("positions", DefaultFrameBuffer["WorldPos"]);
            backBufferShader.SetTexture("uvs", DefaultFrameBuffer["UVs"]);
            backBufferShader.SetTexture("matDataA", DefaultFrameBuffer["MaterialDataA"]);
            backBufferShader.SetTexture("matDataB", DefaultFrameBuffer["MaterialDataB"]);

            if (lightShader == null) lightShader = new ShaderProgram(lightMapVShader, lightMapFShader);
            lightShader.SetTexture("normals", DefaultFrameBuffer["Normals"]);
            lightShader.SetTexture("positions", DefaultFrameBuffer["WorldPos"]);
            lightShader.SetTexture("uvs", DefaultFrameBuffer["UVs"]);
            lightShader.SetTexture("matDataA", DefaultFrameBuffer["MaterialDataA"]);
            lightShader.SetTexture("matDataB", DefaultFrameBuffer["MaterialDataB"]);

            if (ssaoShader == null) ssaoShader = new ShaderProgram(ssaoVShader, ssaoFShader);
            ssaoShader.SetTexture("rnm", new Texture("Shaders/SSGI/noise.png"));
            ssaoShader.SetShaderFloat("totStrength", 0.5f);
            ssaoShader.SetShaderFloat("strength", 0.7f);
            ssaoShader.SetShaderFloat("offset", 18.0f);
            ssaoShader.SetShaderFloat("falloff", 50f);
            ssaoShader.SetTexture("normals", DefaultFrameBuffer["Normals"]);
            ssaoShader.SetTexture("positions", DefaultFrameBuffer["WorldPos"]);
            ssaoShader.SetTexture("uvs", DefaultFrameBuffer["UVs"]);
            ssaoShader.SetTexture("matDataA", DefaultFrameBuffer["MaterialDataA"]);
            ssaoShader.SetTexture("matDataB", DefaultFrameBuffer["MaterialDataB"]);
            ssaoShader.SetShaderMatrix("ProjectionMat", Projection);

            if (copyShader == null) copyShader = new ShaderProgram(frameBufferFShader, frameBufferVShader);
            #endregion

            #region initialize passes
            if (backBufferRenderer == null) backBufferRenderer = new FullScreenQuad();
            backBufferRenderer.Materials[0].Shader = backBufferShader;

            if (lightPass != null) lightPass.Dispose();
            lightPass = new RenderPass(width / 2, height / 2, PixelInternalFormat.Rgba16f);
            lightPass.Shader = lightShader;

            if (ssaoPass != null) ssaoPass.Dispose();
            ssaoPass = new RenderPass(width / 2, height / 2, PixelInternalFormat.Rgba8);
            ssaoPass.Diffuse = DefaultFrameBuffer["Color0"];
            ssaoPass.Shader = ssaoShader;

            if (horizontalBlurPass != null) horizontalBlurPass.Dispose();
            horizontalBlurPass = new RenderPass(width / 2, height / 2, PixelInternalFormat.Rgba16f);
            horizontalBlurPass.Shader = horizontalBlur;

            if (verticalBlurPass != null) verticalBlurPass.Dispose();
            verticalBlurPass = new RenderPass(width / 2, height / 2, PixelInternalFormat.Rgba16f);
            verticalBlurPass.Diffuse = horizontalBlurPass.RenderTarget["Color0"];
            verticalBlurPass.Shader = verticalBlur;

            //Late bindings
            backBufferShader.SetTexture("lightMap", lightPass.RenderTarget["Color0"]);
            backBufferShader.SetTexture("ssaoMap", ssaoPass.RenderTarget["Color0"]);
            #endregion
        }

        #region Update Handler
        void window_UpdateFrame(object sender, OpenTK.FrameEventArgs e)
        {
            Stopwatch s = Stopwatch.StartNew();
            Camera.Update(this);
            if (Update != null) Update(this);
            if (MainThreadInvocationQueue.Count != 0)
            {
                //Only execute enough to maintain a stable update rate
                while (s.ElapsedMilliseconds < window.UpdatePeriod && MainThreadInvocationQueue.Count > 0)
                {
                    MainThreadInvocationQueue.Dequeue()();
                }
            }
            s.Stop();
#if PERFORMANCE_TEST_MODE
            Console.WriteLine("window_UpdateFrame : " + s.ElapsedMilliseconds.ToString());
#endif
        }
        #endregion

        #region Render Handler

        void window_RenderFrame(object sender, OpenTK.FrameEventArgs e)
        {
            window.MakeCurrent();
            Clear(0, 1, 0, 0);

            DefaultFrameBuffer.Bind();

            if (Render != null) Render(this);

            ErrorCode error = GL.GetError();
            if (error != ErrorCode.NoError) throw new Exception(error.ToString());
            #region Lighting
            if (Lighting)
            {
                lightPass.Clear(this, 0, 0, 0, 0);
                ssaoPass.Clear(this, 0, 0, 0, 0);
                horizontalBlurPass.Clear(this, 0, 0, 0, 0);
                verticalBlurPass.Clear(this, 0, 0, 0, 0);

                //Enable additive blending
                GL.Enable(EnableCap.Blend);
                GL.BlendFunc(BlendingFactorSrc.One, BlendingFactorDest.One);

                int lid = 0;

                //First render all directional lights
                for (int i = 0; i < Lights.Count; i++)
                {

                    if (lid == 3 || Lights.Count <= 3)
                    {
                        lightPass.Shader = lightShader;
                        lightPass.Diffuse = DefaultFrameBuffer["Color0"];
                        lightPass.Apply(this);

                        lid = 0;
                    }

                    //if directional light
                    if (Lights[i].GetType() == typeof(DirectionalLight))
                    {
                        lightShader.SetShaderVector("lightColor" + lid, Lights[i].Color);
                        var tmp = (DirectionalLight)Lights[i];
                        lightShader.SetShaderVector("lightDirection" + lid, tmp.Direction);

                        lid++;
                    }
                }

                GL.Disable(EnableCap.Blend);
            }
            #endregion

            #region SSAO
            if (SSAO)
            {
                ssaoShader.SetTexture("lightMap", lightPass.RenderTarget["Color0"]);
                ssaoPass.Diffuse = DefaultFrameBuffer["Color0"];
                ssaoPass.Clear(this, 0, 0, 0, 0);
                ssaoPass.Shader = ssaoShader;
                ssaoPass.Apply(this);
                //horizontalBlurPass.Diffuse = ssaoPass.RenderTarget["Color0"];
                //horizontalBlurPass.Apply(this);
                //verticalBlurPass.Apply(this);
                //ssaoPass.Shader = copyShader;
                //ssaoPass.Diffuse = verticalBlurPass.RenderTarget["Color0"];
                // ssaoPass.Apply(this);
            }
            #endregion

            backBufferShader.SetShaderBool("ssao", SSAO);
            backBufferShader.SetShaderBool("lighting", Lighting);
            backBufferShader.SetShaderVector("ambientLight", AmbientLight);
            backBufferShader.SetShaderVector("cameraPos", Camera.Position);

            //finally apply the passes
            GL.BindFramebuffer(FramebufferTarget.Framebuffer, 0);
            GL.Viewport(0, 0, window.ClientRectangle.Width, window.ClientRectangle.Height);
            backBufferRenderer.Materials[0].Shader = backBufferShader;
            backBufferRenderer.Materials[0].Diffuse = DefaultFrameBuffer["Color0"];
            backBufferRenderer.Draw(this);

            window.SwapBuffers();
        }

        void window_Resize(object sender, EventArgs e)
        {
            MainThreadInvocationQueue.Enqueue(() =>
            {
                int width = (window.WindowState == OpenTK.WindowState.Minimized) ? 10 : window.ClientSize.Width * aaLevel;
                int height = (window.WindowState == OpenTK.WindowState.Minimized) ? 10 : window.ClientSize.Height * aaLevel;

                Projection = Matrix4.CreatePerspectiveFieldOfView(MathHelper.DegreesToRadians(45), (float)width / (float)height, ZNear, ZFar);

                Initialize(width, height);
                //lightPass.RenderTarget.Viewport = new Vector4(0, 0, width/2, height/2);
                //ssaoPass.RenderTarget.Viewport = new Vector4(0, 0, width/2, height/2);
                //DefaultFrameBuffer.Viewport = new Vector4(0, 0, width, height);

            });
        }
        #endregion

        #region Keyboard Handler
        public string[] Keys = new string[1];
        public bool MouseLeftButtonDown { get; private set; }
        public bool MouseRightButtonDown { get; private set; }
        public Vector2 MousePosition
        {
            get
            {
                return new Vector2(window.Mouse.X, window.Mouse.Y);
            }
        }
        public Vector2 MouseDelta
        {
            get
            {
                return new Vector2(window.Mouse.XDelta, window.Mouse.YDelta);
            }
        }
        List<string> keysDown = new List<string>();
        bool alt, shift, control;
        void Keyboard_KeyUp(object sender, OpenTK.Input.KeyboardKeyEventArgs e)
        {
            keysDown.Remove(e.Key.ToString());
            Keys = keysDown.ToArray();
            alt = e.Alt;
            shift = e.Shift;
            control = e.Control;
        }

        void Mouse_ButtonDown(object sender, OpenTK.Input.MouseButtonEventArgs e)
        {
            MouseLeftButtonDown = (e.Button == OpenTK.Input.MouseButton.Left);
            MouseRightButtonDown = (e.Button == OpenTK.Input.MouseButton.Right);
        }

        void Mouse_ButtonUp(object sender, OpenTK.Input.MouseButtonEventArgs e)
        {
            MouseLeftButtonDown = !(e.Button == OpenTK.Input.MouseButton.Left);
            MouseRightButtonDown = !(e.Button == OpenTK.Input.MouseButton.Right);
        }

        void Keyboard_KeyDown(object sender, OpenTK.Input.KeyboardKeyEventArgs e)
        {
            if (!keysDown.Contains(e.Key.ToString())) keysDown.Add(e.Key.ToString());
            Keys = keysDown.ToArray();
            alt = e.Alt;
            shift = e.Shift;
            control = e.Control;
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Start the Engine
        /// </summary>
        /// <param name="frameRate">The requested frame rate</param>
        /// <param name="updateRate">The requested update rate</param>
        public void Start(int frameRate, int updateRate)
        {
            window.Run(frameRate, updateRate);
        }
        /// <summary>
        /// Start the Engine @ 60UPS
        /// </summary>
        /// <param name="frameRate">The requested frame rate</param>
        public void Start(int frameRate)
        {
            Start(frameRate, 0);
        }
        /// <summary>
        /// Start the Engine @ 60FPS and 60UPS
        /// </summary>
        public void Start()
        {
            Start(0);
        }

        public void Clear(float r, float g, float b, float a)
        {
            //TODO maybe it'll be faster to just disable depth testing and draw a fsq? This is currently one of the slowest parts of the engine
            GL.ClearColor(r, g, b, a);
            GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);
        }

        #region Binding Reset
        /// <summary>
        /// Remove the currently bound framebuffer and set the default framebuffer
        /// </summary>
        public void ResetFrameBuffer()
        {
            DefaultFrameBuffer.Bind();
        }
        #endregion

        #region State Machine Properties
        public void EnableDepthTest()
        {
            GL.Enable(EnableCap.DepthTest);
            GL.DepthFunc(DepthFunction.Lequal);
        }

        public void EnableMultisample()
        {
            GL.Enable(EnableCap.Multisample);
        }

        public void DisableMultisample()
        {
            GL.Disable(EnableCap.Multisample);
        }

        public void Transparency(bool mode)
        {
            if (mode)
            {
                GL.Enable(EnableCap.Blend);
                GL.BlendFunc(BlendingFactorSrc.SrcAlpha, BlendingFactorDest.OneMinusSrcAlpha);
            }
            else
            {
                GL.Disable(EnableCap.Blend);
            }
        }

        public void Wireframe(bool mode)
        {
            if (mode)
            {
                GL.PolygonMode(MaterialFace.FrontAndBack, PolygonMode.Line);
            }
            else
            {
                GL.PolygonMode(MaterialFace.FrontAndBack, PolygonMode.Fill);
            }
        }

        public void DepthWrite(bool mode)
        {
            GL.DepthMask(mode);
        }

        public void EnableBlend()
        {
            GL.Enable(EnableCap.Blend);
            GL.BlendFunc(BlendingFactorSrc.SrcAlpha, BlendingFactorDest.OneMinusSrcAlpha);
        }

        public void DisableBlend()
        {
            GL.Disable(EnableCap.Blend);
        }

        public void EnableCullFace()
        {
            GL.Enable(EnableCap.CullFace);
            GL.CullFace(CullFaceMode.Back);
        }

        public void DisableCullFace()
        {
            GL.Disable(EnableCap.CullFace);
        }

        public void DisableDepthTest()
        {
            GL.Disable(EnableCap.DepthTest);
        }

        public void DisableDither()
        {
            GL.Disable(EnableCap.Dither);
        }

        public void EnableDither()
        {
            GL.Enable(EnableCap.Dither);
        }

        #endregion

        public void Dispose()
        {
            //TODO free all loaded resources when they're done

            window.Close();
            window.Dispose();
        }
        #endregion

    }
}
